import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-fullscreen-image',
  templateUrl: './fullscreen-image.page.html',
  styleUrls: ['./fullscreen-image.page.scss'],
})
export class FullscreenImagePage implements OnInit {
  @Input() imageUrl: string;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  dismissModal() {
    this.modalController.dismiss()
  }
}
