import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPostElementsPage } from './new-post-elements.page';

const routes: Routes = [
  {
    path: '',
    component: NewPostElementsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPostPageRoutingModule {}
